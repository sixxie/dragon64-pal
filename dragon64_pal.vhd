-- Dragon 64 PAL

-- 2020 Ciaran Anscomb
-- 2020 Stewart Orchard

-- This implements the glue logic for using the 6847 VDG in the Dragon 64.
-- Most of it is for generating the extra required "padding" lines containing
-- only sync pulses, but there is some associated convenience logic too.

-- To create a PAL(-ish) signal, the clock to the NTSC-generating VDG is
-- stopped at two points in each frame and 25 extra line pulses are introduced
-- each time.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dragon64_pal is
	port (
		     vclk_in : in  std_logic;  -- VClk, SAM pin 7
		     da0_in  : in  std_logic;  -- DA0, VDG pin 22
		     fs      : in  std_logic;  -- ~FS, VDG pin 37
		     hs      : in  std_logic;  -- ~HS, VDG pin 38
		     e       : in  std_logic;  -- E clock, SAM pin 14
		     phase_t : in  std_logic;  -- chroma phase toggle, IC31 pin 4

		     vclk_out  : out std_logic;  -- VClk to VDG pin 33
		     da0_out   : out std_logic;  -- DA0 latched with E, SAM pin 8
		     nhs       : out std_logic;  -- HS latched with E, SAM pin 9, etc.
		     pad_luma  : out std_logic;  -- padding sync lines, R63 RHS (from top)
		     luma_sel  : out std_logic;  -- luma mux select, IC29 (4053) pin 11
		     burst     : out std_logic;  -- colour burst trigger, IC31 ('LS123) pin 2
		     phase_sel : out std_logic   -- chroma phase mux select, IC29 (4053) pin 6
	     );
end;

architecture behaviour of dragon64_pal is

	-- [SO] added initial values for simulation

	-- the non-inverted padding line sync pulse is used internally
	signal pad_hs : std_logic := '0';

	-- internal buffer of phase_sel
	signal phase_sel_reg : std_logic := '0';

	-- VDG line counter
	signal vdg_lcount : unsigned (4 downto 0) := (others => '0');

	-- For selecting where VClk goes
	signal pad_trigger : std_logic := '0';
	signal vclk_select : std_logic := '0';

	-- Clock the padding line generator
	signal pad_vclk : std_logic := '0';

	-- Padding horizontal counter
	signal pad_hcount : unsigned (7 downto 0) := (others => '0');
	signal pad_hcount_reset : std_logic := '0';

	-- Padding line counter
	signal pad_lcounta : unsigned (2 downto 0) := (others => '0');
	signal pad_lcountb : unsigned (2 downto 0) := (others => '0');
	signal pad_lcount_out : std_logic := '0';

	signal fs_reg : std_logic := '0';

begin

	-- Simple latch of HS clocked with E.  Helps keep the signal consistent.
	-- I think early CoCos didn't do this and saw weird problems.
	-- [SO] d32 machines don't have this
	process (e)
	begin
		if (rising_edge(e)) then
			nhs <= hs;
		end if;
	end process;

	-- Similarly, latch DA0 with E.  Also forces DA0 high during FSync,
	-- which I assume has some benefit...

	-- [SO] da0 and start of fs are not delayed, but end of fs is.
	-- for info: da0 high while hsync rises is how sam detects fsync.
	-- d32 uses sam datasheet method: vdg ~fs is connected to ~ms. This
	-- makes da0 float during fsync. sam pulls da0 high via internal
	-- resistor.
	-- d64 uses this register circuit instead of using sam pullup
	process (fs, e)
	begin
		if fs = '0' then
			fs_reg <= '1';  -- start of fs registered immediately
		elsif rising_edge(e) then
			fs_reg <= '0';  -- end of fs delayed by e
		end if;

	end process;

	da0_out <= da0_in or fs_reg;


	-- HSync triggers a short period where øB is routed to the R-Y input of
	-- the LM1889N instead of alternating øA.  I guess this provides the R-Y
	-- component of the colour burst.

	-- [SO] vdg produces chroma burst in øB only, but it needs to be spliced into øA
	-- prior to phase alternation to get the right result for PAL.
	-- 'burst' is actually a fully padded version of hs. This is the nhs
	-- signal in a d32.

	burst <= hs xor pad_hs;

	-- The inverting output from the burst timer chip feeds back in and
	-- toggles the phase alternating of øA.

	process (phase_t)
	begin
		if (rising_edge(phase_t)) then
			phase_sel_reg <= not phase_sel_reg;
		end if;
	end process;

	phase_sel <= phase_sel_reg;

	-- VDG line counter.  When this reaches 24 lines, and then again when
	-- it reaches 32 lines, the exclusive ORing of its output with the
	-- padding line counter will divert VClk to the padding line generator.

	-- [SO] hs falls and fs rises at the same time which is a deal breaker
	-- as far as copying the circuit goes i.e. risk of metastability.
	-- This even violates the 74LS393 datasheet. Reset recovery time is 25ns.
	-- Solution here uses uses delayed version of rising fs from above

	process (fs_reg,hs)
	begin
		if (fs_reg = '0') then
			-- [SO] counter is held in reset while fs high (fs_reg low)
			vdg_lcount <= (others=>'0');
		elsif (falling_edge(hs)) then
			vdg_lcount <= vdg_lcount + 1;
		end if;
	end process;

	-- Padding horizontal counter.  Counts 228 cycles of the redirected
	-- 3.58MHz VClk (or 3.55MHz) before resetting.  The resulting pad_hs
	-- signal is low for the first 16 cycles (simulating a sync pulse) and
	-- high for the rest (representing black).

	process (pad_hcount_reset,pad_vclk)
	begin
		if (pad_hcount_reset = '1') then
			pad_hcount <= (others=>'0');
		elsif (falling_edge(pad_vclk)) then
			pad_hcount <= pad_hcount + 1;
		end if;
	end process;

	pad_hs <= pad_hcount(7) or pad_hcount(6) or
	          pad_hcount(5) or pad_hcount(4);

	pad_hcount_reset <= pad_hcount(7) and pad_hcount(6) and
	                    pad_hcount(5) and pad_hcount(2);

	-- The pad_hs black-and-sync signal is inverted for output as pad_luma
	-- to reflect the inverted Y signal from the VDG.  The luma_sel output
	-- (see below) is used as the select to an analogue mux to affect which
	-- is used.

	pad_luma <= not pad_hs;

	-- Padding line counter.  Clocked by falling edge of pad_hs.  Output
	-- is effectively a divide-by-50; the low 25 and high 25 count the two
	-- different signal interruptions.

	-- Divide-by-5
	-- [SO] synchronous counter
	process (pad_hs)
	begin
		if (falling_edge(pad_hs)) then
			if (pad_lcounta = "100") then
				pad_lcounta <= "000";
			else
				pad_lcounta <= pad_lcounta + 1;
			end if;
		end if;
	end process;

	-- Divide-by-5 clocked by previous divide-by-5
	process (pad_lcounta(2))
	begin
		if (falling_edge(pad_lcounta(2))) then
			if (pad_lcountb = "100") then
				pad_lcountb <= "000";
			else
				pad_lcountb <= pad_lcountb + 1;
			end if;
		end if;
	end process;

	-- Output toggled by second divide-by-5
	process (pad_lcountb(2))
	begin
		if (falling_edge(pad_lcountb(2))) then
			pad_lcount_out <= not pad_lcount_out;
		end if;
	end process;

	-- Redirect VClk to VDG or padding line generator depending on the
	-- current counter states.

	pad_trigger <= not fs and vdg_lcount(3) and vdg_lcount(4);

	process (vclk_in)
	begin
		-- vclk_in inverted before latching, so test falling edge, not rising
		if (falling_edge(vclk_in)) then
			vclk_select <= pad_trigger xor pad_lcount_out;
		end if;
	end process;

	-- Either vclk_out (clock to VDG) or pad_vclk (clock to padding line
	-- generator) will reflect vclk_in depending on vclk_select.

	vclk_out <= vclk_in nand vclk_select;
	pad_vclk <= vclk_in nand not vclk_select;

	-- Output to an analogue mux selects either VDG luma or generated padding line.

	luma_sel <= not vclk_select;

end behaviour;
