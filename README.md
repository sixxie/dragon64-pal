# Dragon 64 PAL logic

VHDL representation of the glue logic for using the 6847 VDG in the Dragon 64.
Most of it is for generating the extra required "padding" lines containing only
sync pulses, but there is some associated convenience logic too.  It should
replace 9 logic chips on the main board with a functional equivalent.

Stewart Orchard has sent fixes to my dubious VHDL, and a testbench!
