GHDL = ghdl
GHDLFLAGS =

ENTITIES = \
	dragon64_pal

TESTBENCHES = \
	dragon64_pal_tb

.PHONY: all
all: testbenches

CLEAN =

###

CLEAN += $(ENTITIES:=.o)

CLEAN += $(TESTBENCHES:=.o)
CLEAN += $(patsubst %,e~%.o,$(TESTBENCHES))
CLEAN += $(TESTBENCHES)
CLEAN += $(TESTBENCHES:=.vcd)

####

%.o: %.vhd
	$(GHDL) -a $(GHDLFLAGS) $<

####

# Testbenches

dragon64_pal_tb.o: dragon64_pal.o

$(TESTBENCHES): %: %.o
	$(GHDL) -e $(GHDLFLAGS) $@

.PHONY: testbenches
testbenches: $(TESTBENCHES)

.PHONY: test
test: testbenches
	./dragon64_pal_tb --vcd=dragon64_pal_tb.vcd

####

.PHONY: clean
clean:
	rm -f $(CLEAN) work-obj*.cf
