-- VHDL test bench for Dragon 64 PAL video logic

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


--  Define an entity without any ports for test bench
entity dragon64_pal_tb is
end dragon64_pal_tb;


architecture behaviour of dragon64_pal_tb is

	-- declare d64 pal logic component
	component dragon64_pal
	port (
		vclk_in : in  std_logic;  -- VClk, SAM pin 7
		da0_in  : in  std_logic;  -- DA0, VDG pin 22
		fs      : in  std_logic;  -- ~FS~, VDG pin 37
		hs      : in  std_logic;  -- ~HS~, VDG pin 38
		e       : in  std_logic;  -- E clock, SAM pin 14
		phase_t : in  std_logic;  -- chroma phase toggle, IC31 pin 4
		vclk_out  : out std_logic;  -- VClk to VDG pin 33
		da0_out   : out std_logic;  -- DA0 latched with E, SAM pin 8
		nhs       : out std_logic;  -- HS latched with E, SAM pin 9, etc.
		pad_luma  : out std_logic;  -- padding sync lines, R63 RHS (from top)
		luma_sel  : out std_logic;  -- luma mux select, IC29 (4053) pin 11
		burst     : out std_logic;  -- colour burst trigger, IC31 ('LS123) pin 2
		phase_sel : out std_logic   -- chroma phase mux select, IC29 (4053) pin 6
	     );
        end component;

	-- test bench signals
	signal vclk_in : std_logic := '0';
        signal e : std_logic := '0';
        signal phase_t : std_logic := '1';
        signal da0_in : std_logic := '0';
        signal fs : std_logic := '1';
        signal hs : std_logic := '1';
        signal vclk_out, da0_out, nhs, pad_luma, luma_sel, burst, phase_sel : std_logic;
       
	-- define SAM clock period
	constant SAMCLKPERIOD: time := 1.0 sec / 14318000.0;
        
        -- define number of scanlines for simulation
        constant SCANLINES: integer := 312*3;

begin
	-- instantiate d64 pal logic component
	d64_pal: dragon64_pal port map (
		vclk_in   => vclk_in,
		da0_in    => da0_in,
		fs        => fs,
		hs        => hs,
		e         => e,
		phase_t   => phase_t,
		vclk_out  => vclk_out,
		da0_out   => da0_out,
		nhs       => nhs,
		pad_luma  => pad_luma,
		luma_sel  => luma_sel,
		burst     => burst,
		phase_sel => phase_sel
	);
        


	-- simulate e & vclk from SAM
        -- requires da0 input to perform vdg synchronisation
        process
                variable samphase: integer range 0 to 15 := 0;
                variable vclk_div: unsigned(1 downto 0) := "00";
                variable vclk_enable: std_logic := '1';
                variable old_da0: std_logic := '0';
        begin
                for i in 1 to SCANLINES*57*16 loop
                        
                        if samphase = 7 then
                                e <= '1' after 55 ns; -- from sam datasheet
                        elsif samphase = 15 then
                                e <= '0' after 25 ns; -- from sam datasheet
                        end if;

                        wait for SAMCLKPERIOD * 0.5;    -- vclk is updated on opposite sam clkin edge relative to e
                        
                        if vclk_div(1) = '1' then
                                vclk_in <= '1' after 50 ns; -- from sam datasheet
                        else
                                vclk_in <= '0' after 65 ns; -- from sam datasheet
                        end if;

                        -- move da0 rising edge to correct timing point by pausing vclk
                        if (da0_out > old_da0) and ((samphase < 10) or (samphase > 11)) then
                                vclk_enable := '0';
                        elsif samphase = 11 then
                                vclk_enable := '1';
                        end if;
                        
                        if vclk_enable = '1' then
                                vclk_div := vclk_div + 1;
                        end if;

                        wait for SAMCLKPERIOD * 0.5;
                        
                        old_da0 := da0_out;
                        samphase := (samphase + 1) mod 16;
		end loop;

		wait;	-- wait forever (terminates simulation)
	end process;
        
        
        -- simulate hs, fs, da0 from 6847 vdg
        -- timings adjusted to approximate measurements from d32 MKII
        process (vclk_out)
		variable vclk_count: integer := 0;
                variable line_count: integer := 261-32-2; -- start a couple of lines before fsync
        begin
                if rising_edge(vclk_out) then
                        
                        if vclk_count = 0 then

                                hs <= '0' after 300 ns; -- datasheet 550 ns max
                                da0_in <= '0' after 500 ns;   -- datasheet does not specify max

                                if line_count = (261-32) then
                                        fs <= '0' after 300 ns; -- same time as falling hs. datasheet 520 ns max
                                elsif line_count = 261 then
                                        fs <= '1' after 300 ns; -- same time as falling hs. datasheet 600 ns max
                                end if;

                                if line_count = 261 then
                                        line_count := 0;
                                else
                                        line_count := line_count + 1;
                                end if;

                        elsif vclk_count = 17 then
                                hs <= '1' after 490 ns; -- datasheet 740 ns max
                        elsif (vclk_count >= 59) and (vclk_count < (59+41*4)) and ((vclk_count mod 4) = 3) then
                                if (line_count > (261-32-192)) and (line_count <= (261-32)) then
                                        da0_in <= not da0_in after 250 ns;      -- datasheet 490 ns max
                                end if;
                        end if;

                        if vclk_count = 227 then
                                vclk_count := 0;
                        else
                                vclk_count := vclk_count + 1;
                        end if;
                end if;
	end process;


        -- simulate IC31a monostable (chroma burst window)
        -- could be synthesised with a vclk counter. hint hint ;)
        process(burst)
        begin
                if rising_edge(burst) then
                        phase_t <= '0', '1' after 3.5 us; -- d64 ISS3 3.5us / d32 MKII 3.7us
                end if;
        end process;
        

end behaviour;
